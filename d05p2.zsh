#!/bin/env zsh
# Advent of Code 2019 - Day 5 Part 2
emulate -RL zsh
setopt KSH_ARRAYS

declare -a mem
declare -a prg

die() {
    echo >&2 ERROR: $*
    exit 5
}

dbg() {
    # echo >&2 $*
}

# Load program from file
# $1 - path
load() {
    declare IFS=,
    read -A prg <$1 || die "Could not read program file"
    unset IFS
}

# Dump memory to stdout
dump() {
    echo ${(j:,:)mem[@]}
}

# Resolve parameter value
# $1 = param
# $2 = mode (1 = immediate, 0 = position)
value() {
    if [[ $2 == 1 ]] then
        echo $1
    else
        echo ${mem[$1]}
    fi
}

run() {
    integer pc=0
    while [[ ${mem[$pc]} != 99 ]]; do
        integer ins=${mem[$pc]}
        integer op=$((ins % 100))
        declare m=${(l:3::0:)$((ins / 100))}
        dbg -n "DBG pc: $pc op: $op modes: $m"
        case $op in
            [1278]) # 3 params
                v1=$(value ${mem[$pc+1]} ${m[-1]})
                v2=$(value ${mem[$pc+2]} ${m[-2]})
                dst=${mem[$pc+3]}
                dbg " v1: $v1 (${mem[$pc+1]}) v2: $v2 (${mem[$pc+2]}) dst: $dst"
                case $op in
                    1)  # Add
                        mem[$dst]=$((v1 + v2));;
                    2)  # Multiply
                        mem[$dst]=$((v1 * v2));;
                    7)  # Less than
                        mem[$dst]=$((v1 < v2));;
                    8)  # Equals
                        mem[$dst]=$((v1 == v2));;
                esac
                pc+=4;;
            [56]) # 2 params
                v1=$(value ${mem[$pc+1]} ${m[-1]})
                v2=$(value ${mem[$pc+2]} ${m[-2]})
                dbg " v1: $v1 (${mem[$pc+1]}) v2: $v2 (${mem[$pc+2]})"
                case $op in
                    5)  # Jump if non-zero
                        [[ $v1 != 0 ]] && pc=$v2 || pc+=3;;
                    6)  # Jump if zero
                        [[ $v1 == 0 ]] && pc=$v2 || pc+=3;;
                esac
                ;;
            [34])
                p1=${mem[$pc+1]}
                dbg " p1: $p1"
                case $op in
                    3)  # Read
                        integer val
                        read val || die "Could not read input"
                        mem[$p1]=$val
                        ;;
                    4)  # Write
                        value $p1 ${m[-1]}
                        ;;
                esac
                pc+=2;;
            *) 
                die "Unknown opcode $op"
        esac
    done
}

if (( $#* < 1 )) then
    echo >&2 "Usage: $0 [PROGRAM_FILE] < INPUT > OUTPUT"
    exit 2
fi

load $1
mem=(${prg[@]})
run

