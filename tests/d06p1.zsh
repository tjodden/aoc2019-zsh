#!/bin/env zsh
emulate -RL zsh

expected=42
dir=${0:h}
cmd=$dir/../${0:t}
result=$(cat $dir/${${0:t}:r}.txt | $cmd)
if [[ $result != $expected ]]; then
    echo >&2 "Error: Expected $expected but got $result"
    exit 1
fi
