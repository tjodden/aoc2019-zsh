#!/bin/env zsh
# Advent of Code 2019 - Day 1 Part 1
emulate -RL zsh
autoload -U zmathfunc && zmathfunc

integer total=0
declare line
while read -sr line; do
    total+=$((max($line / 3 - 2, 0)))
done
echo $total
