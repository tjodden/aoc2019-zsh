#!/bin/env zsh
# Advent of Code 2019 - Day 1 Part 2
emulate -RL zsh
autoload -U zmathfunc && zmathfunc

integer total=0
declare line
while read -sr line; do
    integer mass=$line
    while [[ $mass -gt 0 ]]; do
        integer fuel=$((max($mass / 3 - 2, 0)))
        total+=$fuel
        mass=$fuel
    done
done
echo $total
