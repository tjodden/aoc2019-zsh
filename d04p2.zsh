#!/bin/env zsh
# Advent of Code 2019 - Day 4 Part 1
emulate -RL zsh
# setopt extended_glob
# zmodload zsh/mathfunc

read range
integer min=${${(s:-:)range}[1]}
integer max=${${(s:-:)range}[2]}

check_increasing() {
    for ((i=2; i<=$#1; i++)) do
        if (( ${1[$i]} < ${1[$i-1]} )) then
            return 1
        fi
    done
}

check_pair() {
    integer count=1

    for ((i=1; i<=$#num; i++)) do
        if [[ ${1[$i]} == ${1[$i-1]:-X} ]] then
            count+=1
        else
            if [[ $count == 2 ]] then
                return 0
            fi
            count=1
        fi
    done

    if [[ $count == 2 ]] then
        return 0
    fi

    return 1
}

integer count=0
for ((num=min; num<=max; num++)) do
    if check_increasing $num && check_pair $num; then
        count+=1
    fi
done

echo $count
