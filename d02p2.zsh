#!/bin/env zsh
# Advent of Code 2019 - Day 2 Part 2
emulate -RL zsh
setopt ksh_arrays # Start array indexes from 0, not 1.

declare -a mem

run() {
    integer pc=0
    while [[ ${mem[$pc]} != 99 ]]; do
        integer op=${mem[$pc]}
        integer a=${mem[$pc+1]}
        integer b=${mem[$pc+2]}
        integer dst=${mem[$pc+3]}
        # echo >&2 pc: $pc op: $op a: $a b: $b dst: $dst
        case $op in
            1) mem[$dst]=$((${mem[$a]} + ${mem[$b]}));;
            2) mem[$dst]=$((${mem[$a]} * ${mem[$b]}));;
            *) echo >&2 "Error: Unknown opcode $op" && exit 1
        esac
        pc+=4
    done
}

# Load program
declare -a prg
IFS=,
read -A prg
unset IFS

# Find noun and verb
declare needle=19690720 
for noun in $(seq 0 99); do
    for verb in $(seq 0 99); do
        mem=(${prg[@]})
        mem[1]=$noun
        mem[2]=$verb
        run
        if [[ ${mem[0]} == $needle ]]; then
            echo $((100 * $noun + $verb))
            exit 0
        fi
    done
done

# Not found
echo >&2 "No solution found :("
exit 1

