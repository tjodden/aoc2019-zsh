#!/bin/env zsh
# Advent of Code 2019 - Day 4 Part 1
emulate -RL zsh
# setopt extended_glob
# zmodload zsh/mathfunc

read range
integer min=${${(s:-:)range}[1]}
integer max=${${(s:-:)range}[2]}

integer count=0
for ((num=min; num<=max; num++)) do
    is_increasing=1
    has_double=0

    for ((i=2; i<=$#num; i++)) do
        if (($num[$i] < $num[$i-1])) then
            is_increasing=0
            break
        fi
        if (($num[$i] == $num[$i-1])) then
            has_double=1
        fi
    done
    
    if ((is_increasing && has_double)) then
        ((count++))
    fi
done

echo $count
