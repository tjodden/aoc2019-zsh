#!/bin/env zsh
# Advent of Code 2019 - Day 2 Part 1
emulate -RL zsh
setopt ksh_arrays # Start array indexes from 0, not 1.

declare -a mem

# Load program
IFS=,
read -A mem

# Set special memory values
mem[1]=12
mem[2]=2

# Evaluate
integer pc=0
while [[ ${mem[$pc]} != 99 ]]; do
    integer op=${mem[$pc]}
    integer a=${mem[$pc+1]}
    integer b=${mem[$pc+2]}
    integer dst=${mem[$pc+3]}
    # echo >&2 pc: $pc op: $op a: $a b: $b dst: $dst
    case $op in
        1) mem[$dst]=$((${mem[$a]} + ${mem[$b]}));;
        2) mem[$dst]=$((${mem[$a]} * ${mem[$b]}));;
        *) echo >&2 "Error: Unknown opcode $op" && exit 1
    esac
    pc+=4
done

# Result
echo ${mem[0]}
