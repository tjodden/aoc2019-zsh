#!/bin/env zsh
# Advent of Code 2019 - Day 6 Part 1
emulate -RL zsh

declare -A map

load() {
    declare -a line
    declare IFS=")"
    while read -A line; do
        map[$line[2]]=$line[1]
    done
    unset IFS
}

load

declare me=$map[YOU]
declare to=$map[SAN]
# echo >&2 "Find route from $me to $to"

integer count=0
while [[ -n $me ]]; do
    declare tmp=$to

    # Check if there is a route from santa to my current position,
    # by going from santa inwards.
    integer subcount=0
    while [[ -n $tmp && $tmp != $me ]]; do
        # echo >&2 "Check from $tmp to $map[$tmp]" 
        tmp=$map[$tmp]
        subcount+=1
    done

    if [[ $tmp == $me ]]; then
        count+=$subcount
        break
    fi

    # echo >&2 "Transfer me inwards from $me to $map[$me]"
    me=$map[$me]
    count+=1
done

if [[ -z $me ]]; then
    echo >&2 "No route found :("
    exit 1
fi

# echo >&2 "Route found \o/"
echo $count
