#!/bin/env zsh
# Advent of Code 2019 - Day 3 Part 2
emulate -RL zsh
setopt extended_glob
zmodload zsh/mathfunc

read_wire() {
    declare IFS=,
    read -A $1
    return $?
}

declare -A trace

echo >&2 Loading and tracing wires...
declare -a wire
integer wire_id=0
while read_wire wire; do
    ((x=0, y=0, steps=0, wire_id++))
    for cmd in $wire; do
        ((dx=0, dy=0))
        case $cmd[1] in
            U) dy=+1;;
            D) dy=-1;;
            R) dx=+1;;
            L) dx=-1;;
            *) echo >&2 Unknown direction; exit 1
        esac
        integer len=$cmd[2,-1]
        while ((len--)) do
            ((x+=dx, y+=dy, steps++))
            value=$trace[$x,$y]
            if [[ $value != *${wire_id}=* ]] then
                trace[$x,$y]+=${value:+,}${wire_id}=$steps
            fi
        done
    done
done

echo >&2 Finding intersections and calculating combined number of steps...
declare -a dists
for k v in ${(kv)trace}; do
    if [[ $v == *,* ]] then
        integer x=${${(s:,:)k}[1]}
        integer y=${${(s:,:)k}[2]}
        integer s1=${${=v//[,=]/ }[2]}
        integer s2=${${=v//[,=]/ }[4]}
        dists+=$((s1+s2))
    fi
done

echo >&2 Finding intersection with fewest number of steps...
echo ${${(n)dists}[1]}
