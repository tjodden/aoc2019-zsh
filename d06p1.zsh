#!/bin/env zsh
# Advent of Code 2019 - Day 6 Part 1
emulate -RL zsh

declare -A map

load() {
    declare -a line
    declare IFS=")"
    while read -A line; do
        map[$line[2]]=$line[1]
    done
    unset IFS
}

load

integer count=0
for object in ${(k)map}; do
    count+=1
    while [[ $map[$object] != "COM" ]]; do
        object=$map[$object]
        count+=1
    done
done

echo $count
